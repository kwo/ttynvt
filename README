ttynvt makes a virtual serial device (tty) and connects
the device to a Network Virtual Terminal (NVT).

                 +-ttynvt-+------------------------+
                 |        |\                       |
                 |        | +---------------+      |
                 |        |                 |      |
                 |        |                 |      |
   /dev/ttyNVT   |       pty   /dev/ppp    pty     |
        |        |      slave      |      master   |
        |        |        |        |        |      |
   --------------------------------------------------
        |        |        |        |        |      |
        |        |        |        |        |      |
        +--FUSE--+        +-tty--ldisc------+     TCP--->host


1 Build environment
  gcc is required.
  glibc must support pthreads.
  libfuse - Filesystem in Userspace - development support is needed.

2 Target
  The linux kernel at the target system must support FUSE (FUSE_FS=y) and
  libpthread and libfuse must be installed in the root filesystem.

3 Build
  $ autoreconf -vif
  Example configuration for native build:
  $ ./configure
  Example configuration for cross build:
  $ ./configure --host=ARCH-unknown-linux \
      CC=PATH-TO-CROSS-GCC \
      PKG_CONFIG_LIBDIR=PATH-TO-CROSS-PKG_CONFIG_LIBDIR"
  or
  $ ./configure --host=ARCH-unknown-linux \
      CC=PATH-TO-CROSS-GCC \
      LDFLAGS="-L PATH-TO-LOCATION-OF-libfuse.so"
  $ make

  The default target in the Makefile builds the Network Virtual Terminal.
  Output name is: ttynvt.

4 Run
  The application (ttynvt) installs a virtual serial device in the /dev
  directory (by default: /dev/ttyNVT0).
  The major/minor number of the device, the address (name/IP) and the port
  number of the serial server must be specified when the application
  is started (command-line options).

  E.g.
  ./ttynvt -M 199 -m 6 -n ttyNVT0 -S 169.254.1.1:5020

  To get a list of all command-line options:
  ./ttynvt --help

5. Test
   The serial server: ser2net (http://ser2net.sourceforge.net/) might be used
   to test the virtual serial server on the target.
   Install the server (ser2net) and add following line to the configuration
   file (/etc/ser2net.conf):
   5020:telnet:0:/dev/ttyS0:115200 remctl NONE 1STOPBIT 8DATABITS RTSCTS HANGUP_WHEN_DONE
   At the server, plug-in a loopback connector in the serial port (ttyS0).
   At the target, open the virtual serial port, with a termial program,
   e.g minicom:
   minicom -D /dev/ttyNVT0

6 Diagnostic
  Add the command-line option '-D <level>' to enable logging of diagnostic information.
  The log is forwarded to syslog with the debug level.
  Wireshark is also very useful when debugging the interface between the client
  and the server.

Ref: RFC-854  - Telnet Protocol Specification
     RFC-2217 - Telnet Com Port Control Option
