/*
 * Copyright (c) 2018 Lars Thrane A/S
 * SPDX-License-Identifier: GPL-2.0
 *
 * Telnet interface - server
 */
#ifndef TELNET_SERVER_H
#define TELNET_SERVER_H

typedef void    (cli_tx_f) (void *cctx, const void *buf, int len);
typedef void    (cli_ss_f) (void *cctx, int signal, int value);

/* Telnet context */
typedef struct {
    void           *cctx;       /* Client context */

    cli_tx_f       *cli_tx;     /* Write to client */
    cli_ss_f       *cli_ss;     /* Signal from client */

    unsigned int    bytes_rx;   /* Rx byte count */
    unsigned int    off_rx;     /* Rx buffer offset */
    unsigned char   mode_telnet;        /* Telnet mode */
    unsigned char   mode_rfc2217;       /* RFC-2217 mode */

    unsigned int    baudrate;
    unsigned char   datasize;
    unsigned char   parity;
    unsigned char   stopsize;

    unsigned char   linestate;
    unsigned char   modemstate;

    unsigned char   linestate_mask;
    unsigned char   modemstate_mask;

    char            dtr_state;
    char            rts_state;
} tn_ctx_t;

extern tn_ctx_t *telnet_ctx_init(void *cctx, cli_tx_f * ftx, cli_ss_f * fss);

extern int      telnet_rx(tn_ctx_t * tcc, char *buf, int *plen);
extern int      telnet_tx(tn_ctx_t * tcc, const char *buf, int len);

extern void     telnet_reply_opt(tn_ctx_t * tcc, int opt, int prm,
                                 const void *val, int len);

extern void     telnet_rfc2217_init(tn_ctx_t * tcc, int when);
extern void     telnet_rfc2217_handle_opt(tn_ctx_t * tcc, int opt,
                                          unsigned char *pu, int nd);

extern void     telnet_rfc2217_change_linestate(tn_ctx_t * tcc, int mask);
extern void     telnet_rfc2217_change_modemstate(tn_ctx_t * tcc, int mask);

/* Init modes */
#define TNI_INIT    0           /* Init context     */
#define TNI_ON      1           /* Option activated */

/* Signals that may be passed to cli_ss_f() */
#define TNS_DTR     1
#define TNS_RTS     2

#endif /* TELNET_SERVER_H */
