#ifndef TEST_H
#define TEST_H 1

#include <gtest/gtest.h>

#include "config.h"

#define D(...)  do{ if (debug)     printf(__VA_ARGS__); }while(0)
#define D2(...) do{ if (debug > 1) printf(__VA_ARGS__); }while(0)

extern int      debug;

#define COL_RST  "\x1B[0m"
#define COL_RED  "\x1B[31m"
#define COL_GRN  "\x1B[32m"
#define COL_YEL  "\x1B[33m"
#define COL_BLU  "\x1B[34m"
#define COL_MAG  "\x1B[35m"
#define COL_CYN  "\x1B[36m"
#define COL_WHT  "\x1B[37m"

void            pr_text(const char *col, const char *fmt, ...);
void            pr_info(const char *fmt, ...);

#endif /* TEST_H */
