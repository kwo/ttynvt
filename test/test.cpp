/*
 * General test stuff
 *
 * - The main() function
 * - Some printing functions
 */
#include "test.h"

int             debug = 0;

static bool     ttyout = false;

int main(int argc, char **argv)
{
    const char     *s;

    ttyout = isatty(STDOUT_FILENO);

    ::testing::InitGoogleTest(&argc, argv);

    for (argc--, argv++; argc > 0; argc--, argv++)
    {
        s = argv[0];
        if (*s++ != '-')
            break;
      again:
        switch (*s++)
        {
        case 'd':
            debug++;
            goto again;
        }
    }

    return RUN_ALL_TESTS();
}

#include <stdarg.h>

void _pr_text(const char *col, const char *fmt, va_list args)
{
    char            fmtx[1024];

    if (ttyout)
        snprintf(fmtx, sizeof(fmtx), "%s[          ] -  %s%s\n",
                 col, fmt, COL_RST);
    else
        snprintf(fmtx, sizeof(fmtx), "[          ] -  %s\n", fmt);
    fmt = fmtx;

    vprintf(fmt, args);
}

void pr_text(const char *col, const char *fmt, ...)
{
    va_list         args;

    va_start(args, fmt);
    _pr_text(col, fmt, args);
    va_end(args);
}

void pr_info(const char *fmt, ...)
{
    va_list         args;

    va_start(args, fmt);
    _pr_text(COL_YEL, fmt, args);
    va_end(args);
}
